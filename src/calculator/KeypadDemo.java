package calculator;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;
import javax.swing.border.BevelBorder;

import calculator.model.BinaryOperator;
import calculator.model.Calculator;

/**
 * Demonstrate use of the Keypad component.
 */
public class KeypadDemo extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final String OPERATORS = "[-+*/^]";
	private static final String EQUAL = "=";
	private static JTextField display;
	private Calculator calc;
	private Map<String,BinaryOperator> ops;
	private static String number = "";
	private state currentState = state.APPEND;
	private enum state{
		APPEND {
			@Override
			public void display(String s) {
				number += s;
				display.setText(display.getText()+s);
			}
		},
		SET {
			@Override
			public void display(String s) {
				number += s;
				display.setText(s);
			}
		};
		public abstract void display(String s);
	}
	public KeypadDemo() {
		calc = new Calculator();
		ops = new HashMap<String,BinaryOperator>();
		for(BinaryOperator op: calc.getOperators()) ops.put(op.name, op);
		
		this.setTitle("keypad test");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// create some actions for keypad.
		// the actions just print when a key is pressed
		String[] keys = new String[] { "1", "2", "3", "/", "4", "5", "6", "*",
				"7", "8", "9", "-", ".", "0", "=", "+" };
		Action[] actions = new Action[keys.length];
		for (int k = 0; k < keys.length; k++) {
			if (OPERATORS.contains(keys[k])){
				actions[k] = new AbstractAction(keys[k]) {

					@Override
					public void actionPerformed(ActionEvent e) {
						calc.setValue(Double.parseDouble(number));
						String key = (String) this.getValue(NAME);
						calc.setOperator(ops.get(key));
						display.setText(String.valueOf(calc.getValue()));
						number = "0";
						currentState = state.SET;
					}
				};
			}
			else if(EQUAL.equals(keys[k])){
				actions[k] = new AbstractAction(keys[k]) {

					@Override
					public void actionPerformed(ActionEvent e) {
						// AbstractAction contains a map of key-value pairs with
						// several pre-defined keys.
						// NAME returns the name of this Action.
						calc.setValue(Double.parseDouble(number));
						calc.perform();
						display.setText(String.valueOf(calc.getValue()));
						number = "0";
						currentState = state.SET;
					}
				};
			}
			else{
				actions[k] = new AbstractAction(keys[k]) {

					@Override
					public void actionPerformed(ActionEvent e) {
						// AbstractAction contains a map of key-value pairs with
						// several pre-defined keys.
						// NAME returns the name of this Action.
						String key = (String) this.getValue(NAME);
//						System.out.println(key);
						currentState.display(key);
						currentState = state.APPEND;
					}
				};
			}
		}
		// make a keypad
		Keypad keypad = new Keypad(4, 4);
		keypad.setKeys(actions);
		this.getContentPane().add(keypad, BorderLayout.CENTER);

		display = new JTextField(12);
		display.setEditable(false);
		display.setBorder(new BevelBorder(BevelBorder.LOWERED));
		display.setHorizontalAlignment(JTextField.RIGHT);
		this.getContentPane().add(display, BorderLayout.NORTH);
		this.pack();
	}

	private void append(String s) {
		number += s;
		display.setText(display.getText()+s);
	}

	public void run() {
		this.setVisible(true);
	}

	public static void main(String[] args) {
		Runnable demo = new Runnable() {
			public void run() {
				new KeypadDemo().run();
			}
		};
		SwingUtilities.invokeLater(demo);
	}

}
