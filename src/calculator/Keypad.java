package calculator;
import java.awt.event.*;
import java.awt.GridLayout;
import javax.swing.*;
/**
 * A Keypad is a JComponent with a grid of keys (buttons).
 * Each key is programmed using an Action.
 * You create a keypad with Action objects, such as:
 * int rows = 4;
 * int cols = 3;
 * Keypad keypad = new Keypad(rows, cols, actions);
 */
public class Keypad extends JPanel
{
    /** a null action you can apply to unused keys, does nothing. */
    public static final Action NULL_ACTION;
    /** space (pixels) between rows and columns in keypad */
    public static int GAP = 4;
    /** rows in the keypad */
    private int rows = 1;
    /** columns in the keypad */
    private int cols = 1;
    /** the actions for the keys */
    private Action[] actions;
    /** initialize the null action to disabled */
    static {
    	NULL_ACTION = new NullAction(" ");
    	NULL_ACTION.setEnabled(false);
    }
    
    /**
     * A Keypad with only one key.
     */
    public Keypad()
    {
        this(1,1);
    }
    
    /** 
     * Create a keypad with given number of rows and columns.
     * The keys don't do anything.
     * @param rows number of rows in keypad
     * @param cols number of columns in keypad
     */
    public Keypad(int rows, int cols) {
        assert rows > 0;
        assert cols > 0;
        this.rows = rows;
        this.cols = cols;
        // create do-nothing actions
        actions = new Action[rows*cols];
        for(int k=0; k<rows*cols; k++)
            actions[k] = NULL_ACTION; // new NullAction( Integer.toString(k+1) );
        initComponents();
    }
    
    /** 
     *  Create a new keypad with a grid of buttons.
     *  Each button has an Action assigned to it.
     *  @param rows is the rows in the keypad
     *  @param cols in the columns in the keypad
     *  @param actions is an array of actions to assign to buttons.
     *     You should have rows*columns >= size of actions array.
     *     Unused spaces on keypad will be blank.
     *  @prerequisite the actions are not null.
     */
    public Keypad(int rows, int cols, Action[] actions) {
        assert rows > 0;
        assert cols > 0;
        assert actions != null;
        this.cols = cols;
        this.rows = rows;
        this.actions = actions;
        initComponents();
    }
    
    public void initComponents() {
        // remove the old components
        this.removeAll();
        // create keys
        GridLayout layout = new GridLayout(rows,cols);
        layout.setHgap(GAP);
        layout.setVgap(GAP);
        this.setLayout( layout );
        for(int k=0; k<actions.length; k++) {
            JButton key = new JButton( actions[k] );
            this.add(key);
        }
        // use keys with null action for any remaining keys?
        int size = rows*cols;
        for(int k=actions.length; k<size; k++) {
        	this.add( new JButton(NULL_ACTION));
        }
    }
    
    /**
     * Set the key actions.
     * The number of rows in layout is adjusted to be big enough
     * to hold all the actions.
     */
    public void setKeys(Action [] actions) {
        this.actions = actions;
        rows = (int)Math.ceil(1.0*actions.length/cols);
        if (rows == 0) rows = 1;
        initComponents();
    }
    
    /* a do-nothing action */
    static class NullAction extends AbstractAction {
        public NullAction(String name) { super(name); }
        public void actionPerformed(ActionEvent evt) {
            /* do nothing */
            //System.out.println( super.getValue(Action.NAME) );
        }
    }
}
