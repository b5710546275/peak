package calculator.model;

/**
 * The logic component for a calculator.
 * This calculator has 2 registers for saving data: reg1 and reg2.
 * reg1 is the first operand and also stores results of binary operations.
 * reg2 is the second operand for binary operations.
 * currentRegister is 1 or 2 if the "current" register for data is reg1 or reg2.
 * Example:
 * setValue(12)        reg1=12
 * setOperand('+')     operand = +, 
 *                     currentRegister=2
 * setValue(9)         reg2=9
 * perform( )          reg1= reg1 operand reg2 = 12+9
 *                     currentRegister=1
 * 
 * This is a deliberately bad design.
 * Apply some modeling and design patterns to make it better.
 */
public class Calculator extends java.util.Observable {

	private static final BinaryOperator NOOP; // no operation pending
	// available operators
	private static final BinaryOperator[] ops;
	private double reg1 = 0;
	private double reg2 = 0;
	private int currentRegister = 1;
	//private Operator op = new calculator.operator.Noop();
	private BinaryOperator operator;
	
	static {
		NOOP = new BinaryOperator("") {
			@Override
			public double perform(double arg1, double arg2) {
				return arg1;
			}
		};
		// initialize known operators
		ops = new BinaryOperator[5];
		ops[0] = new BinaryOperator("+") {
			public double perform(double arg1, double arg2) { return arg1+arg2; }
		};
		ops[1] = new BinaryOperator("-") {
			public double perform(double arg1, double arg2) { return arg1-arg2; }
		};
		ops[2] = new BinaryOperator("*") {
			public double perform(double arg1, double arg2) { return arg1*arg2; }
		};
		ops[3] = new BinaryOperator("/") {
			public double perform(double arg1, double arg2) { return arg1/arg2; }
		};
		ops[4] = new BinaryOperator("^") {
			public double perform(double arg1, double arg2) { return Math.pow(arg1,arg2); }
		};
	}
	
	public Calculator( ) {
		reg1 = reg2 = 0.0;
		currentRegister = 1;
		operator = NOOP;
	}
	
	/**
	 * Handle numeric key press.
	 * @param key is a number key as a char, '0', '1', ..., '9'
	 */
	public void setValue(double value) {
		if (currentRegister==1) reg1 = value;
		else reg2 = value;
	}
	
	public double getValue( ) {
//		if (currentRegister==1) return reg1;
//		else return reg2;
		return reg1;
	}
	
	/**
	 * The the operation to perform, but don't actually perform it yet.
	 * The operation is performed when perform() is invoked.
	 * This enables handling of infix notation, such as 2 + 3.
	 * @param operator is the operator to be performed
	 */
	public void setOperator(BinaryOperator operator) {
		// if we have already set register2 then perform pending binary operator
		if (currentRegister == 2) perform();
		this.operator = operator;
		// after an operator is input, the next value always goes in register 2
		currentRegister = 2;
	}
	
	public BinaryOperator getOperator() {
		return operator;
	}
	
	/**
	 * Perform operation, if any
	 */
	public void perform( ) {
		reg1 = operator.perform(reg1, reg2);
		//System.out.println("\n============\n"+reg1+" "+reg2+" "+operator.name);
		currentRegister = 1;
		//TODO notify observers
	}
	
	public static BinaryOperator[] getOperators() {
		return ops;
	}
}