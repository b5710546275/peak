package calculator.model;

/**
 * This is step 2 of the calculator project.
 * Use BinaryOperator as a superclass for operations
 * the calculator can perform.
 * 
 */
public abstract class BinaryOperator {
	public final String name;
	/**
	 * Initialize a new BinaryOperator
	 * @param name a display name for user interface such as "*"
	 */
	public BinaryOperator(String name) {
		this.name = name;
	}
	
	/**
	 * Perform operation and return a result.
	 * @param arg1 is first operand to use
	 * @param arg2 is second operand
	 * @return result of operation on arg1 and arg2
	 */
	public abstract double perform(double arg1, double arg2);
	
}
