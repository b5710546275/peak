package calculator;
import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * A lame attempt at a keypad component.
 * This keypad is configured by providing strings and ActionListeners for the keys.
 * Its not a good encapsulation of the intention (purpose) of the keys.
 */
public class LameKeypad extends JPanel {
	
	private String[] keys = {"1","2","3","4","5","6","7","8","9","*","0","DEL"};
	private JButton[] buttons;
	private int rows;
	private int columns;

	/** default constructor */
	public LameKeypad() {
		this(4,3);  // default size
	}
	
	public LameKeypad(int rows, int cols)  {
		this.rows = (rows>0)? rows : 1;
		this.columns = (cols>0)? cols : 1;
		initComponents();
	}
	
	private void initComponents() {
		int size = rows * columns;
		buttons = new JButton[ size ];
		this.setLayout( new GridLayout(rows,columns));
		// create empty buttons
		for(int k=0; k<size; k++) {
			buttons[k] = new JButton( );
			this.add(buttons[k]);
		}
		// set default labels
		setKeys( keys );
	}

	public String[] getKeys() { 
		return keys;
	}
	public String getKeys(int index) { 
		if ( keys != null && index < keys.length) return keys[index]; else return null; 
	}

	public void setKeys( String[] keys) { 
		this.keys = keys;
		int length = Math.min(buttons.length , keys.length);
		for(int k=0; k < length; k++) {
			buttons[k].setText(keys[k]);
			buttons[k].setEnabled(true);
		}
		// disable unused keys
		for(int k=length; k<buttons.length; k++) {
			buttons[k].setText(" ");
			buttons[k].setEnabled(false);
		}
	}
	
	public void setKeys(int index, String key) { 
		if ( keys != null && index < keys.length) {
			this.keys[index] = key;
			if ( buttons != null ) buttons[index].setText(key);
		}
	}
	
	public void addActionListener( ActionListener listener ) {
		for(JButton b: buttons) b.addActionListener(listener);
	}
	
	public void removeActionListener( ActionListener listener ) {
		for(JButton b: buttons) b.removeActionListener(listener);
	}
}

