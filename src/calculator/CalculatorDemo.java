package calculator;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import calculator.model.BinaryOperator;
import calculator.model.Calculator;

/**
 * demo use of Calculator class via command line.
 * The calculator is intended to have a GUI interface,
 * but invoking calculator directly promotes testing.
 * @author jim
 *
 */
public class CalculatorDemo {
	private static final String OPERATORS = "[-+*/^]";
	private static final String EQUALS = "=";
	private static Calculator calc;
	private static Map<String,BinaryOperator> ops;
	static {
		calc = new Calculator();
		ops = new HashMap<String,BinaryOperator>();
		for(BinaryOperator op: calc.getOperators()) ops.put(op.name, op);
	}
	
	/** parse an input expression and give it to calculator.  Echo each argument to console. */
	public static void parseAndExecute(String expression) {
		Scanner scanner = new Scanner(expression);
		// input must consist of alternating numbers and operators.
		while( scanner.hasNext()) {
			if (scanner.hasNextDouble()) {
				double value = scanner.nextDouble();
				System.out.print(value); 
				System.out.print(' ');
				calc.setValue(value);
			}
			else if (scanner.hasNext(OPERATORS)) {
				String opcode = scanner.next(OPERATORS);
				System.out.print(opcode);
				System.out.print(' ');
				calc.setOperator(ops.get(opcode));
			}
			else if (scanner.hasNext(EQUALS)) {
				scanner.next(EQUALS); // consume it
				System.out.print("= ");
				calc.perform();
				System.out.println(calc.getValue());
			}
			else {
				System.out.println("Unparsable value: " + scanner.next());
				return;
			}
		}
		scanner.close();
		
		
	}
	
	public static void main(String[] args) {
		calc = new Calculator();
		
		// this is a dumb parser. You must separate operators by space
		parseAndExecute("2 + 3 =");
		parseAndExecute("5 * 7 =");
		parseAndExecute("10 ^ 3 =");
		parseAndExecute("1 + 2 + 3 =");
		
		System.out.println("\nHow many seconds in a year?");
		parseAndExecute("365.25 * 24 * 60 * 60 =");
		
	}

}