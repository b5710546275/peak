# Design and Implement a Calculator

A calculator provides a good opportunity to practice design
and use several design patterns.  We will write the calculator
in steps. At least step we will add new functionality and
improve the design.

## 0. Model How the Calculator Works

To get started, try the basic Windows calculator.
Here is an online calculator that is a good example:    
http://www.online-calculator.com/

What happens if you enter: `4` `x` `5` `=` `=` `=`

Can you make a "model" for how this calculator works?

It seems to have 2 registers for storing operands,
and a way of remembering the current operator (+, -, x, etc).
It always performs pending operations before accepting
a new operation.

## 1. Simple Calculator

Starter code is in the master branch of this repository:
https://jbrucker@bitbucket.org/skeoop/calculator
(Git: git@bitbucket.org:skeoop/calculator.git)


### calculator.model.Calculator.java
The calculator has 2 registers and can perform + - * / and ^ (exponentiation).

The interface is:
```
void setValue(double arg) - set a register value
double getValue( )        - get current register value
void setOperand(char op)  - set operation to perform, '+', '-', '*', '/', '^'
void perform( )           - perform operator and save result (to register 1)
                            use getValue() to get result
```



### CalculatorDemo.java
Demo the use of calculator using a console UI. This lets us verify the model is working.
The demo can parse expressions like: `2 + 3 * 5 =` and invoke calculator methods.

### Exercise
The Calculator has a problem.  `2 + 3 * 5` should be 25 but it is not.
Fix the Calculator class.

## 2. BinaryOperators as objects
The calculator is using a `char` variable for operators and a switch statement
to perform the operation.   Apply the _Command Pattern_. 
Define command objects for operators.  Use a superclass named `BinaryOperator`.
An operator should also have `name` which is not shown in this diagram.

![BinaryOperator.png](https://bitbucket.org/repo/6XR568/images/334887279-BinaryOperator.png)
Represent operators as objects.


### Exercises
2.1 Define BinaryOperator as abstract class.
2.2 Write operators for Add, Subtract, Multiply, Divide, and Power.
2.3 Modify the Calculator class to use these operators instead of `char`.
2.4 What design pattern is this?

Starter code: the `operator` branch of this project.   
a. First have a look at the `operator` branch:
```
git checkout operator
```
b. If you like it, merge the operator branch into the master branch. You have to switch back to the master branch and then issue a merge command.  It won't be easy because the demo class is different.  You can use copy-and-paste instead.
```
git checkout master
git merge operator
```

### NullObject Pattern
It makes the code messy and hard to read to constantly check for nulls:
```
if (operator == null) {
    do nothing
}
else {
    reg1 = operator.perform(reg1, reg2);
}
```
A solution is do define a "do nothing" object.  This is called the **Null Object Pattern**.
Then you can omit the checks for null.  In the Calculator code there is a NOOP binary operator
that does nothing.


## 3. Design UI and Controller

Once the calculator is working with operators defined, its time to design a graphical UI
and a controller class.

### 3.1 Design the UI and Controller

### 3.2 Implement a simple UI with Keypad

You can use this keypad component: https://bitbucket.org/skeoop/keypad/

### 3.3 Implement the Controller

The Controller's responsbilities are:

|handle Digit Key Press | handleDigitKey( key ) |
|handle Enter or = Key Press | handleEnter( )   |

`handle Digit Key Press` - when user presses a digit or ".", then add this to the input value and update the display.  Ignore invalid input, such as too many leading '0' or more than one '.' key.

`handle Enter` - when user pressed "=" or "Enter" key, submit the number to calculator.

This uses the MVC pattern.  Verify that the keypad and display work.

## 4. Add Operators

Add another keypad for '+', '-', '*', '-', '^' binary operators.  Add it to the UI.

You need to use the Adapter and Command patterns for this.    
The Keypad wants Action objects for the keys.
But the Calculator wants BinaryOperator objects.
Write an OperatorAction (adapter) to encapsulate a BinaryOperator
so it can be used on the Keypad.